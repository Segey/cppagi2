### Cpp agi2

You will ask me: "How make this stuff?"
I will tell you: "It is very simple;"

1) Edit "main.C", and write your script in it;
---main.cpp ---

  #include "cppagi2/cppagi2.cpp"

  int main()
  {
     agi a;

     if( a.cmd("stream file hello-world 0") < 0)
     {
       // cerr << a.err;
       return -1;
     }
     a.cmd("hangup");

     return 0;
  }

------------
2) Compile binary file, move it to agi-bin/

  # make
  # mv build/sr.bin /path/to/agi-bin/  [/usr/share/asterisk/agi-bin]
  # make clean

3) Edit "extensions.conf"

  exten => _09,1,AGI(sr.bin)
  exten => _09,n,Hangup()

4) Reload Dialplan, then call 09

###
