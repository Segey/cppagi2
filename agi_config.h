/**
 * \file      cppagi/agi_consts.h 
 * \brief     The Agi_consts class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 27(th), 2019, 13:47 MSK
 * \updated   October   (the) 27(th), 2019, 13:47 MSK
 * \TODO      
**/
#pragma once

#define AGI_CODEOK 200
#define AGI_SOUNDSPATH "/var/lib/asterisk/sounds"
#define AGI_ERROR 3
#define AGI_WARNING 6
#define AGI_INFO 9
#define AGI_UNUSED(x) ((void)x)

#ifdef DEBUG
#define AGI_LOG(msg, verbosity) log(msg,verbosity,__FILE__,__FUNCTION__,__LINE__)
#define AGI_THROW(msg) throw error(agiobj, msg, __FILE__, __FUNCTION__, __LINE__)
#else
#define AGI_LOG(msg, verbosity) log(msg, verbosity)
#define AGI_THROW(msg) throw error(msg)
#endif
