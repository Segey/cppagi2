/**
 * \file      cppagi/agi_exception.h 
 * \brief     The Agi_exception class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 27(th), 2019, 13:29 MSK
 * \updated   October   (the) 27(th), 2019, 13:29 MSK
 * \TODO      
**/
#pragma once
#include <string>
#include <stdexcept>

// AGI exception class
class agi_exception: public std::exception {
 public:
    using class_name = agi_exception;
    using inherited = std::exception;

 private:
    std::string m_msg;

 public:
    agi_exception(std::string const& msg = {}) noexcept
        : std::exception()
        , m_msg(msg){
	} 
    virtual ~agi_exception() override = default;
    virtual const char* what() const noexcept override final {
        return m_msg.c_str();
	}
};
