#include "agi_error.h"
#include "cppagi2.h"

agi_error::agi_error(agi& agiobj, std::string const& msg, std::string const& file, std::string const& function, int line)
        : agi_exception(msg)
        , m_agiobj(&agiobj)
        , m_file(file) 
        , m_function(function) 
        , m_line(line) {
#ifdef DEBUG
    m_debug = true;
    agiobj->log(m_msg, 0, m_file, m_function, m_line);
#else
    m_file = "";
    m_function = "";
    m_agiobj->log(msg, AGI_ERROR);
#endif
}
