#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "agi_config.h"
#include "agi_exception.h"
#include "agi_hangup.h"
#include "agi_error.h"

using namespace std;

class agi {
    using class_name = agi;

private:
    std::vector<std::string> environment;
    std::ostringstream request;
    std::string response;
    std::string dtmf_buffer;
    std::string execCommand;
	int code;
    int result;
	bool hangup_detected;

public:
	
    // basic methods
	agi();
	virtual ~agi();
	void clear(void);
	int exec(const std::string &command);
	int read(void);
	int getCode(void);
	int getResult(void);
    std::string getData(void);
    std::string getEnv(const std::string &varname);
	bool isConnected(void);	
	bool fail(void);
	void log(const std::string& message, int verbosity=AGI_INFO, const std::string &file="", 
		const std::string &function="", int line=0);
	agi_error error( const std::string& msg, const std::string& file="", 	
		const std::string& function="", int line=0);

	// class methods
	static std::string ltime(time_t t=0);
	static std::string upper(const std::string &s);
    static std::string sprintf(std::string fmt, ...);

	// agi commands
    void verbose(std::string const& message, int verbosity = 3);
	void answer(void);
	void play(const std::string &file, 
		const std::string valid_digits="1234567890*#");
    void record( const std::string &file, std::string const& format = "gsm",
		int timeout=5000, int silence=0);
    std::string getDtmf(int length=1, int timeout=4000, char terminator='#');
    std::string input(const std::string &prompt, int length=1, int timeout=4000, 
		char terminator='#');
	char menu( const std::string& prompt, const std::string& options, 
               const std::string& error = {}, const std::string& retry_message="", int retry=3);
	void jump(const std::string &context, const std::string &extension, int priority=1);
	void sayDigits(const std::string &digits);
	void sayNumber(const std::string &number);
	void saySetsOfTen(const std::string &digits);
    void sayTime(std::string timestr = "NOW");
	bool hangup(int cause=17);
	int getChannelStatus(const std::string &channel="");
	void setCallerId(const std::string &cid);
	void setLanguage(const std::string &language="br");
    std::string getLanguage(void);
    std::string dbGet(const std::string family, const std::string key);
	void dbPut(const std::string family, const std::string key, const std::string value);
	void dbDel(const std::string family, const std::string key);
    std::string getVar(const std::string &var);
	void setVar(const std::string &var, const std::string &value);
};


