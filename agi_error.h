/**
 * \file      cppagi/agi_error.h 
 * \brief     The Agi_error class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 27(th), 2019, 13:38 MSK
 * \updated   October   (the) 27(th), 2019, 13:38 MSK
 * \TODO      
**/
#pragma once
#include "agi_config.h"
#include "agi_exception.h"

class agi;

class agi_error: public agi_exception {
public:
    using class_name = agi_error;
    using inherited  = agi_exception;

private:
    agi* m_agiobj = nullptr;
    std::string m_file;
    std::string m_function;
	int m_line = 0;
	bool m_debug = false;

public:
    agi_error(agi& agiobj, std::string const& msg, std::string const& file = {}, std::string const& function = {}, int line = 0);
    virtual ~agi_error() noexcept override = default;
    std::string const& getFile() const {
        return m_file;
    }
    std::string const& getFunction() const {
        return m_function;
    }
    int getLine() const {
        return m_line;
    }
};
