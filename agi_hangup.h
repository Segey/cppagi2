/**
 * \file      cppagi/agi_hangup.h 
 * \brief     The Agi_hangup class provides 
 * \author    S.Panin <dix75@mail.ru>
 * \copyright S.Panin, 2006 - 2019 
 * \version   v.1.0
 * \created   October   (the) 27(th), 2019, 13:35 MSK
 * \updated   October   (the) 27(th), 2019, 13:35 MSK
 * \TODO      
**/
#pragma once
#include "agi_exception.h"

class agi_hangup: public agi_exception {
 public:
     using class_name = agi_hangup;
     using inherited  = agi_exception;

 public:
	agi_hangup(std::string const& msg) noexcept
        : agi_exception(msg) {
    }
};
